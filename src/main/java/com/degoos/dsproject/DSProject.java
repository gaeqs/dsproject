package com.degoos.dsproject;

import com.degoos.dsproject.interfaces.Game;
import com.degoos.dsproject.object.GameImpl;

/**
 * This class was created for testing purposes.
 */
public class DSProject {

	/**
	 * Project's main method.
	 *
	 * @param args the arguments. This param is not used.
	 */
	public static void main(String[] args) {
		Game game = new GameImpl();
		game.start(2);
	}

}
