package com.degoos.dsproject.object;

import com.degoos.dsproject.interfaces.Token;

public class TokenImpl implements Token {

	private int leftValue, rightValue;

	public TokenImpl(int leftValue, int rightValue) {
		this.leftValue = leftValue;
		this.rightValue = rightValue;
	}

	@Override
	public int getLeftValue() {
		return leftValue;
	}

	@Override
	public int getRightValue() {
		return rightValue;
	}

	@Override
	public void flip() {
		int aux = leftValue;
		leftValue = rightValue;
		rightValue = aux;
	}

	@Override
	public boolean containsValue(int value) {
		return leftValue == value || rightValue == value;
	}

	@Override
	public boolean is66() {
		return leftValue == rightValue && leftValue == 6;
	}

	@Override
	public void print() {
		System.out.print("[" + leftValue + "|" + rightValue + "]");
	}
}
