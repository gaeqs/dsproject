package com.degoos.dsproject.object;

import com.degoos.dsproject.interfaces.Player;
import com.degoos.dsproject.interfaces.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class PlayerImpl implements Player {

	private int id;
	private List<Token> tokens;

	public PlayerImpl(int id, int tokens) {
		this.id = id;
		this.tokens = new ArrayList<>(tokens);
	}

	@Override
	public int getPlayerID() {
		return id;
	}

	@Override
	public Optional<Token> getToken(int index) {
		return index < 0 || index >= tokens.size() ? Optional.empty() : Optional.of(tokens.get(index));
	}

	@Override
	public void addToken(Token token) {
		tokens.add(token);
	}

	@Override
	public Token removeToken(int index) {
		return tokens.remove(index);
	}

	@Override
	public int getTokenAmount() {
		return tokens.size();
	}

	@Override
	public boolean hasTokens() {
		return !tokens.isEmpty();
	}

	@Override
	public void printTokens() {
		tokens.forEach(Token::print);
		System.out.println();
	}

	@Override
	public void printTokensIDs() {
		System.out.print("  0");
		for (int i = 1; i < tokens.size(); i++)
			System.out.print("    " + i);
		System.out.println();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PlayerImpl player = (PlayerImpl) o;
		return id == player.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
