package com.degoos.dsproject.object;

import com.degoos.dsproject.enums.EnumCommandResult;
import com.degoos.dsproject.interfaces.Board;
import com.degoos.dsproject.interfaces.Player;
import com.degoos.dsproject.interfaces.Token;
import com.degoos.dsproject.interfaces.TokenStack;
import com.degoos.dsproject.util.NumericUtils;

import java.util.Optional;

/**
 * Returns the status of a command.
 */
public class CommandStatus {

	private Optional<Token> token;
	private int index;
	private EnumCommandResult commandResult;


	public CommandStatus(TokenStack stack, Board board, Player player, String command) {
		if (command.isEmpty()) {
			token = Optional.empty();
			index = -1;
			commandResult = EnumCommandResult.EMPTY;
		} else if (command.equals("k")) {
			token = Optional.empty();
			index = -1;
			commandResult = EnumCommandResult.VALID;
		} else if (command.equals("s")) {
			token = Optional.empty();
			index = -1;
			commandResult = stack.isEmpty() ? EnumCommandResult.EMPTY_STACK : EnumCommandResult.VALID;
		} else if (!NumericUtils.isInteger(command)) {
			token = Optional.empty();
			index = -1;
			commandResult = EnumCommandResult.INVALID;
		} else {
			index = Integer.valueOf(command);
			token = player.getToken(index);
			if (!token.isPresent()) commandResult = EnumCommandResult.INCORRECT_INDEX;
			else if (!board.canBePlaced(token.get())) commandResult = EnumCommandResult.CANNOT_PLACE;
			else commandResult = EnumCommandResult.VALID;
		}
	}

	public Optional<Token> getToken() {
		return token;
	}

	public int getIndex() {
		return index;
	}

	public EnumCommandResult getCommandResult() {
		return commandResult;
	}
}
