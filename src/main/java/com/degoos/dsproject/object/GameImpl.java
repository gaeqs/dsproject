package com.degoos.dsproject.object;

import com.degoos.dsproject.enums.EnumCommandResult;
import com.degoos.dsproject.interfaces.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class GameImpl implements Game {

	private static final int MAX_TOKEN_VALUE = 6;
	private static final int MAX_TOKEN_VALUE_1 = MAX_TOKEN_VALUE + 1;
	private static final int TOKEN_AMOUNT = MAX_TOKEN_VALUE_1 * (MAX_TOKEN_VALUE + 1) / 2;

	private Scanner scanner;

	private List<Player> players;
	private Board board;
	private TokenStack stack;

	private int round;
	private int currentPlayer;

	private Player winner;
	private int score;

	/**
	 * Creates a new GameImpl instance and instances all required objects.
	 */
	public GameImpl() {
		scanner = new Scanner(System.in);
		players = new ArrayList<>();
		board = new BoardImpl(MAX_TOKEN_VALUE_1);
		stack = new TokenStackImpl();
		round = 0;
		currentPlayer = -1;
		winner = null;
	}


	@Override
	public void start(int playerAmount) {
		String command;
		do {
			prepare(playerAmount);

			startRoundLoop();

			System.out.println();
			do {
				System.out.print("Do you want to play again? Y/N: ");
				command = scanner.nextLine().toLowerCase();
			} while (!command.equals("y") && !command.equals("n"));
			if (command.equals("y")) {
				reset();
			}
		} while (command.equals("y"));
	}

	/**
	 * Prepares this game before play.
	 * It creates all players, fills the token stack and adds all players the amount of tokens on start.
	 * This amount of tokens is calculates by the amount of tokens divided by the number of players.
	 * The maximum amount of tokens by players is {@link #MAX_TOKEN_VALUE_1}.
	 *
	 * @param playerAmount
	 */
	private void prepare(int playerAmount) {
		for (int i = 1; i <= playerAmount; i++)
			players.add(new PlayerImpl(i, TOKEN_AMOUNT));
		stack.fill();
		int tokensPerPlayer = Math.min(TOKEN_AMOUNT / players.size(), MAX_TOKEN_VALUE_1);
		for (Player player : players) {
			for (int i = 0; i < tokensPerPlayer; i++) {
				player.addToken(stack.pop());
			}
		}
	}

	/**
	 * Resets this game.
	 */
	private void reset() {
		players.clear();
		board.reset();
		stack.clear();
		round = 0;
		currentPlayer = -1;
		winner = null;
	}

	/**
	 * This method starts the round loop. It will loop until a winner is given.
	 * Then, it will print the winner and their score.
	 */
	private void startRoundLoop() {
		Player player;

		while (winner == null) {
			round++;
			currentPlayer++;
			if (currentPlayer == players.size()) currentPlayer = 0;
			player = players.get(currentPlayer);
			round(player);
		}

		System.out.println("Player " + winner.getPlayerID() + " wins the game! They scored " + score + " points!");
	}

	/**
	 * Plays a round.
	 *
	 * @param player the player of the round.
	 */
	private void round(Player player) {
		printRoundInfo(player);
		getCommand(player);
	}

	/**
	 * Prints the game's status.
	 *
	 * @param player the player of the current round.
	 */
	private void printRoundInfo(Player player) {
		System.out.println();
		System.out.println("-----------------------------------------------------------------");
		System.out.println();
		System.out.println("Round " + round + " (Player " + player.getPlayerID() + ")");
		System.out.println("The stack contains " + stack.size() + " tokens.");
		System.out.print("Board: ");
		board.printTokens();
		System.out.print("Tokens: ");
		player.printTokens();
		System.out.print("Tokens: ");
		player.printTokensIDs();
	}

	/**
	 * Asks the player a command and parses it.
	 *
	 * @param player the player to ask.
	 */
	private void getCommand(Player player) {
		String command;
		CommandStatus status;
		EnumCommandResult result;
		System.out.print("Insert the token id, S to steal or K to skip this round: ");
		do {
			command = scanner.nextLine().toLowerCase();
			status = new CommandStatus(stack, board, player, command);
			result = status.getCommandResult();

			if (result == EnumCommandResult.INVALID)
				System.out.print("Invalid command! Please insert a valid command: ");
			if (result == EnumCommandResult.EMPTY_STACK)
				System.out.print("You cannot steal more tokens because the stack is empty! Skip or insert an index: ");
			if (result == EnumCommandResult.INCORRECT_INDEX)
				System.out.print("Incorrect index! Please insert a valid index or insert S to steal or K to skip: ");
			if (result == EnumCommandResult.CANNOT_PLACE)
				System.out.print("This token cannot be placed in the board! Please select another token or insert S or K: ");
		} while (result != EnumCommandResult.VALID);

		if (command.equals("k")) {
			System.out.println("Player " + player.getPlayerID() + " skipped their round!");
			return;
		}

		if (command.equals("s")) {
			Token token = stack.pop();
			player.addToken(token);
			System.out.print("You stole the token ");
			token.print();
			System.out.println("!");
			return;
		}

		playToken(player, status.getToken().get(), status.getIndex());
	}


	/**
	 * Plays the given token.
	 *
	 * @param player the player of this round.
	 * @param token  the token to play.
	 * @param index  the index of the token.
	 */
	private void playToken(Player player, Token token, int index) {
		if (token.is66()) {
			System.out.println("The token has been placed automatically!");
			board.insertAtEnd(token);
		} else {
			if (!token.containsValue(board.getFirstToken().getLeftValue())) {
				board.insertAtEnd(token);
				System.out.println("The token has been placed automatically at the end!");
			} else if (!token.containsValue(board.getLastToken().getRightValue())) {
				board.insertAtStart(token);
				System.out.println("The token has been placed automatically at the start!");
			} else {
				System.out.print("Insert S if you want to place the token at the start or E if you want to place it at the end: ");
				String command;
				do {
					command = scanner.nextLine().toLowerCase();
					if (!command.equals("s") && !command.equals("e")) {
						System.out.print("Invalid command! Please insert S or E: ");
					}
				} while (!command.equals("s") && !command.equals("e"));

				if (command.equals("s"))
					board.insertAtStart(token);
				else board.insertAtEnd(token);
			}
		}
		player.removeToken(index);
		checkWin(player);
	}

	/**
	 * Check whether there is a winner.
	 *
	 * @param player the player of this round.
	 */
	private void checkWin(Player player) {
		if (!player.hasTokens()) {
			winner = player;
			calculateScore();
		} else {
			int value = board.getFirstToken().getLeftValue();
			if (value == board.getLastToken().getRightValue() && board.getTokenAmountWithValue(value) == MAX_TOKEN_VALUE_1 + 1) {
				winner = players.stream().min(Comparator.comparingInt(Player::getTokenAmount)).orElse(player);
				calculateScore();
			}
		}
	}

	/**
	 * Calculates the winner's score.
	 */
	private void calculateScore() {
		for (Player loser : players) {
			if (!loser.equals(winner))
				score += loser.getTokenAmount();
		}
		score -= winner.getTokenAmount() * (players.size() - 1);

	}
}
