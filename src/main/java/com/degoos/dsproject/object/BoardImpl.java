package com.degoos.dsproject.object;

import com.degoos.dsproject.interfaces.Board;
import com.degoos.dsproject.interfaces.Token;

import java.util.*;

public class BoardImpl implements Board {

	private LinkedList<Token> tokens;
	protected Map<Integer, Integer> valueMap;

	public BoardImpl(int values) {
		tokens = new LinkedList<>();
		valueMap = new HashMap<>(values);
	}

	@Override
	public Token getFirstToken() {
		return tokens.getFirst();
	}

	@Override
	public Token getLastToken() {
		return tokens.getLast();
	}

	@Override
	public void insertAtStart(Token token) {
		if (!isEmpty()) {
			Token first = getFirstToken();
			if (first.getLeftValue() != token.getRightValue()) token.flip();
		}
		tokens.addFirst(token);
		addValuesToMap(token);
	}

	@Override
	public void insertAtEnd(Token token) {
		if (!isEmpty()) {
			Token last = getLastToken();
			if (last.getRightValue() != token.getLeftValue()) token.flip();
		}
		tokens.addLast(token);
		addValuesToMap(token);
	}

	@Override
	public int getTokenAmountWithValue(int value) {
		return valueMap.getOrDefault(value, 0);
	}

	@Override
	public boolean isEmpty() {
		return tokens.isEmpty();
	}

	@Override
	public boolean canBePlaced(Token token) {
		if (tokens.isEmpty()) return token.is66();
		return token.containsValue(getFirstToken().getLeftValue())
				|| token.containsValue(getLastToken().getRightValue());
	}

	@Override
	public void printTokens() {
		tokens.forEach(Token::print);
		System.out.println();
	}

	@Override
	public void reset() {
		tokens.clear();
		valueMap.clear();
	}

	private void addValuesToMap(Token token) {
		addValueToMap(token.getRightValue());
		addValueToMap(token.getLeftValue());
	}

	private void addValueToMap(int value) {
		valueMap.put(value, getTokenAmountWithValue(value) + 1);
	}
}
