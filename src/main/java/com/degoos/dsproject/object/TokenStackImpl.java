package com.degoos.dsproject.object;

import com.degoos.dsproject.interfaces.Token;
import com.degoos.dsproject.interfaces.TokenStack;

import java.util.Collections;
import java.util.Stack;

public class TokenStackImpl implements TokenStack {

	private Stack<Token> stack;


	public TokenStackImpl() {
		stack = new Stack<>();
	}

	public void fill() {
		for (int x = 0; x < 6; x++) {
			for (int y = x; y < 7; y++) {
				stack.push(new TokenImpl(x, y));
			}
		}
		Collections.shuffle(stack);
		stack.push(new TokenImpl(6, 6));
	}

	@Override
	public Token pop() {
		return stack.pop();
	}

	@Override
	public int size() {
		return stack.size();
	}

	@Override
	public boolean isEmpty() {
		return stack.isEmpty();
	}

	@Override
	public void clear() {
		stack.clear();
	}
}
