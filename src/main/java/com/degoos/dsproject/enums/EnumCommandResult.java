package com.degoos.dsproject.enums;

/**
 * This enum represents the result of a command check.
 */
public enum EnumCommandResult {

	VALID, EMPTY, INVALID, EMPTY_STACK, INCORRECT_INDEX, CANNOT_PLACE

}
