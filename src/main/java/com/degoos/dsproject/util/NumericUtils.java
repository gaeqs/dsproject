package com.degoos.dsproject.util;

public class NumericUtils {

	/**
	 * Returns whether the given string is an {@link Integer}.
	 *
	 * @param string the string to check.
	 * @return whether the given string is an {@link Integer}.
	 */
	public static boolean isInteger(String string) {
		try {
			Integer.valueOf(string);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

}
