package com.degoos.dsproject.interfaces;

import java.util.Optional;

/**
 * This interface represents a player.
 */
public interface Player {

	/**
	 * Returns the ID of this player.
	 *
	 * @return the ID of this player.
	 */
	int getPlayerID();

	/**
	 * Returns the {@link Token} at the given index, or {@link Optional#empty()} if not present.
	 *
	 * @param index the index.
	 * @return the {@link Token}, or {@link Optional#empty()} if not present.
	 */
	Optional<Token> getToken(int index);

	/**
	 * Adds a {@link Token} at the end of the player's list.
	 *
	 * @param token the {@link Token} to add.
	 */
	void addToken(Token token);

	/**
	 * Returns the amount of {@link Token}s the player has.
	 *
	 * @return the amount of {@link Token}s.
	 */
	int getTokenAmount();

	/**
	 * Removes a {@link Token} from the player list and returns it or {@code null} if not present.
	 *
	 * @param index the index of the {@link Token}.
	 * @return the {@link Token} or null if not present.
	 */
	Token removeToken(int index);

	/**
	 * Returns whether this player has any {@link Token}s.
	 *
	 * @return whether this player has any {@link Token}s.
	 */
	boolean hasTokens();

	/**
	 * Print all player's {@link Token}s.
	 */
	void printTokens();

	/**
	 * Print all player's {@link Token}s' id.
	 */
	void printTokensIDs();

}
