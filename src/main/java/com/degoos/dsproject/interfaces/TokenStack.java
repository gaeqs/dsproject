package com.degoos.dsproject.interfaces;

/**
 * This interface represents a token stack. This object stores all unused tokens in a game.
 */
public interface TokenStack {

	/**
	 * Fills this stack with new {@link Token}s. The 6-6 {@link Token} will be always the first in the stack.
	 */
	void fill();

	/**
	 * Returns the first {@link Token} on the stack.
	 *
	 * @return the first {@link Token} on the stack.
	 */
	Token pop();

	/**
	 * Returns the amount of {@link Token}s this stack has.
	 *
	 * @return the amount of {@link Token}s.
	 */
	int size();

	/**
	 * Returns whether this stack is empty.
	 *
	 * @return whether this stack is empty.
	 */
	boolean isEmpty();

	/**
	 * Clears this stack.
	 */
	void clear();
}
