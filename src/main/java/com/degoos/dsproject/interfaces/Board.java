package com.degoos.dsproject.interfaces;

import java.util.NoSuchElementException;

/**
 * This interface represents a domino's board.
 */
public interface Board {

	/**
	 * Returns the first {@link Token} in the board, or throws a {@link NoSuchElementException} if not present.
	 *
	 * @return the first {@link Token} in the board, or throws a {@link NoSuchElementException} if not present.
	 */
	Token getFirstToken();

	/**
	 * Returns the last {@link Token} in the board, or throws a {@link NoSuchElementException} if not present.
	 *
	 * @return the last {@link Token} in the board, or throws a {@link NoSuchElementException} if not present.
	 */
	Token getLastToken();

	/**
	 * Inserts the given {@link Token} at the start of the board.
	 *
	 * @param token the {@link Token} to insert.
	 */
	void insertAtStart(Token token);

	/**
	 * Inserts the given {@link Token} at the end of the board.
	 *
	 * @param token the {@link Token} to insert.
	 */
	void insertAtEnd(Token token);

	/**
	 * Returns the amount of times a value is represented on the board.
	 *
	 * @param value the value.
	 * @return the amount of times the given value is represented on the board.
	 */
	int getTokenAmountWithValue(int value);

	/**
	 * Returns whether this board is empty.
	 *
	 * @return whether this board is empty.
	 */
	boolean isEmpty();

	/**
	 * Returns whether the given {@link Token} can be placed onto this board.
	 *
	 * @param token the {@link Token}.
	 * @return whether the given {@link Token} can be placed onto this board.
	 */
	boolean canBePlaced(Token token);

	/**
	 * Prints all {@link Token}s on the board.
	 */
	void printTokens();

	void reset();

}
