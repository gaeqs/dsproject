package com.degoos.dsproject.interfaces;

/**
 * This interface represents a game. It's used as the core of any domino's game.
 */
public interface Game {

	/**
	 * Starts the game.
	 *
	 * @param playerAmount the amount of players.
	 */
	void start(int playerAmount);

}
