package com.degoos.dsproject.interfaces;

/**
 * This interface represents a domino's token.
 */
public interface Token {

	/**
	 * Returns the left value of this token.
	 *
	 * @return the left value.
	 */
	int getLeftValue();

	/**
	 * Returns the right value of this token.
	 *
	 * @return the right value.
	 */
	int getRightValue();

	/**
	 * Flips this token. This method toggles the left and right value of this token.
	 */
	void flip();

	/**
	 * Returns whether this token contains the given value.
	 *
	 * @param value the value.
	 * @return whether this token contains the given value.
	 */
	boolean containsValue(int value);

	/**
	 * Returns whether this token's values are equal to 6.
	 *
	 * @return whether this token's values are equal to 6.
	 */
	boolean is66();

	/**
	 * Print this token.
	 */
	void print();
}
